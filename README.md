# BEF Glossary

This module provides glossary links format for better exposed filters.
Glossary Links format similiar ot select Links format.

It is only small tweak to show links as inline blocks.

## Installation

Standard module installation applies. See
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

## Usage

Select 'Glossary Links' format in BEF Settings for taxonomy filter.
